### Problem Statement 8


You are given two arrays of integers of size n. The first array of integers A =  [a<sub>0</sub>, a<sub>1</sub>, …, a<sub>n-1</sub>] is an array in which a<sub>i</sub> s are distinct, and 0 <= a<sub>i</sub> <= n - 1. The second array of integers B = [b<sub>0</sub>, b<sub>1</sub>, b<sub>2</sub>, …, b<sub>n-1</sub>] is an array in which all the b<sub>i</sub>s are distinct, and 0 <= b<sub>i</sub> <= n - 1.

Let A&B define a permutation of the elements of B as follows:
The element in the 0th position of the array [b<sub>0</sub>, b<sub>1</sub>, b<sub>2</sub>, …, b<sub>n-1</sub>] goes to the a<sub>0</sub><sup>th</sup> position, the element in the 1<sup>st</sup> position goes to the a<sub>1</sub><sup>th</sup> position, and so on. In general, the element in the i<sup>th</sup> position of the array [b<sub>0</sub>, b<sub>1</sub>, b<sub>2</sub>, …, b<sub>n-1</sub>] is shifted to the a<sub>i</sub><sup>th</sup> position.

Let C be the new array that results from the permutation.
So C = A&B

Example:

Suppose A = [1, 2, 0, 3, 4] and B = [4, 3, 1, 2, 0]

Then C = A&B = [1, 4, 3, 2, 0]

Now define A&<sup>k</sup>B = A&(A& …&(B)...)  (with k As)

Example:

 A&<sup>3</sup>B = A&(A&(A&B)))

Find the smallest non-zero positive integer j such that B = A&<sup>j</sup>B.

In the example above, j = 3 i.e. A&<sup>3</sup>B = A&(A&(A&B))) = B

**Constraints**

1 <= T <= 10000

1 <=N <= 10<sup>9</sup>

**Input format**

The first line of input consists of an integer T. This is the number of test cases. Then T lines follow with the input format as follows:

N a<sub>0</sub> a<sub>1</sub> … a<sub>n-1</sub> b<sub>0</sub> b<sub>1</sub> … b<sub>n-1</sub>

Here N represents the size of the arrays A and B. a<sub>0</sub>, a<sub>1</sub>, ... , a<sub>n-1</sub> are the N elements of the array A (in order!), while b<sub>0</sub>, b<sub>1</sub>, … b<sub>n-1</sub> are the N elements of B (in order!).

**Output format**

For each test case, print out the integer k where k is the smallest integer such that A&<sup>k</sup>B = B


**Sample Input:**
```
2
5 1 2 0 3 4 4 3 1 2 0
10 1 0 3 2 7 6 9 8 4 5 0 1 2 3 4 5 6 7 8 9 
```

**Sample Output:**
```
3
6
```